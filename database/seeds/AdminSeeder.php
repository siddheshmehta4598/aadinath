<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        $admins = [
    		[
                'name' => 'Admin',
                'email' => "admin@aadinath.com",
                'password' => Hash::make('admin@123'),                
                'active' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
    	];

        DB::table('admins')->insert($admins);
    }
}
