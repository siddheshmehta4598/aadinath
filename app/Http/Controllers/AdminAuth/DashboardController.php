<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class DashboardController extends Controller
{

    public function _construct()
    {
        $this->middleware('guest:admin');
    }
    public function index(Request $request)
    {
        // dd(Auth::guard('admin')->name);
        return view('admin.main');
    }
}
